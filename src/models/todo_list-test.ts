import { expect } from "chai";
import { Maybe } from "tsmonad";
import { unindent } from "../testhelpers/unindent";
import { Item } from "./item";
import { ItemIdentifier } from "./item_identifier";
import { renderTodoList } from "./todo_dsl";
import { TodoList } from "./todo_list";
import { TodoListIdentifier } from "./todo_list_identifier";
import { TodoListName } from "./todo_list_name";

describe("todo list", () => {
  const empty = TodoList.create(
    TodoListIdentifier.fromString("2a"),
    TodoListName.fromString("demo"));

  describe("empty list", () => {
    it("should have an identifier", () => {
      expect(empty.identifier.value).to.equal("2a");
    });

    it("should have a name", () => {
      expect(empty.name.value).to.equal("demo");
    });

    it("should have no item", () => {
      expect(empty.items.size).to.equal(0);
    });
  });

  describe("rename list", () => {
    it("should change the name of the list", () => {
      expect(empty.rename(TodoListName.fromString("new name")).name.value).to.equal("new name");
    });
  });

  describe("create item", () => {
    it("should be able to create new item", () => {
      expect(renderTodoList(empty.createItem("first"))).to.equal(unindent(`
        |[2a] demo:
        |  - [ ] first
      `));
    });

    it("new item should be unckecked", () => {
      expect(valueOf(empty.createItem("first").item(ItemIdentifier.of(0))).isChecked).to.equal(false);
    });
  });

  describe("get item", () => {
    it("should return nothing when requesting item with out of range index", () => {
      expect(Maybe.isNothing(empty.item(ItemIdentifier.of(42)))).to.equal(true);
    });

    it("should return item when requesting item with correct index", () => {
      const todo = empty.createItem("first").createItem("second");
      expect(valueOf(todo.item(ItemIdentifier.of(0))).label).to.equal("first");
      expect(valueOf(todo.item(ItemIdentifier.of(1))).label).to.equal("second");
    });
  });

  describe("delete item", () => {
    it("should do nothing when delete index out of range", () => {
      expect(renderTodoList(empty.createItem("first").delete(ItemIdentifier.of(42)))).to.equal(unindent(`
        |[2a] demo:
        |  - [ ] first
      `));
    });

    it("should delete item with correct index", () => {
      expect(renderTodoList(empty.createItem("first").delete(ItemIdentifier.of(0))))
        .to.equal(unindent(`
          |[2a] demo:
          |  <empty>
        `));
      expect(renderTodoList(empty.createItem("first").createItem("second").delete(ItemIdentifier.of(0))))
        .to.equal(unindent(`
          |[2a] demo:
          |  - [ ] second
        `));
    });
  });

  describe("check item", () => {
    it("should do nothing when delete index out of range", () => {
      expect(renderTodoList(empty.createItem("first").check(ItemIdentifier.of(42))))
        .to.equal(unindent(`
          |[2a] demo:
          |  - [ ] first
        `));
    });

    it("should delete item with correct index", () => {
      expect(renderTodoList(empty.createItem("first").check(ItemIdentifier.of(0))))
        .to.equal(unindent(`
          |[2a] demo:
          |  - [x] first
        `));
      expect(renderTodoList(empty.createItem("first").createItem("second").check(ItemIdentifier.of(0))))
        .to.equal(unindent(`
          |[2a] demo:
          |  - [x] first
          |  - [ ] second
        `));
    });
  });

  describe("uncheck item", () => {
    const listWithFirstChecked = empty.createItem("first").check(ItemIdentifier.of(0));
    it("should do nothing when delete index out of range", () => {
      expect(renderTodoList(listWithFirstChecked.uncheck(ItemIdentifier.of(42))))
        .to.equal(unindent(`
          |[2a] demo:
          |  - [x] first
        `));
    });

    it("should delete item with correct index", () => {
       expect(renderTodoList(listWithFirstChecked.uncheck(ItemIdentifier.of(0))))
         .to.equal(unindent(`
           |[2a] demo:
           |  - [ ] first
         `));
       expect(renderTodoList(listWithFirstChecked.createItem("second").uncheck(ItemIdentifier.of(0))))
       .to.equal(unindent(`
         |[2a] demo:
         |  - [ ] first
         |  - [ ] second
       `));
    });
  });

  describe("toggle item", () => {
    it("should do nothing when delete index out of range", () => {
      expect(renderTodoList(empty.createItem("first").toggle(ItemIdentifier.of(42))))
        .to.equal(unindent(`
          |[2a] demo:
          |  - [ ] first
        `));
    });

    it("should delete item with correct index", () => {
      expect(renderTodoList(empty.createItem("first").toggle(ItemIdentifier.of(0))))
        .to.equal(unindent(`
          |[2a] demo:
          |  - [x] first
        `));
      expect(renderTodoList(empty.createItem("first").toggle(ItemIdentifier.of(0)).toggle(ItemIdentifier.of(0))))
        .to.equal(unindent(`
          |[2a] demo:
          |  - [ ] first
        `));
      expect(renderTodoList(empty.createItem("first").createItem("second").toggle(ItemIdentifier.of(0))))
        .to.equal(unindent(`
          |[2a] demo:
          |  - [x] first
          |  - [ ] second
        `));
    });
  });

  function valueOf(maybe: Maybe<Item>): Item {
    return maybe.valueOr(null);
  }
});
