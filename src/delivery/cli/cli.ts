/* tslint:disable:no-console */
const Help = `
Usage:
  cli list create <name>
  cli list all
  cli list <id> rename <name>
  cli list <id> delete
  cli item create <id> <label>
  cli item delete <id> <index>
  cli item toggle <id> <index>
  cli -h | --help
  cli --version

Options:
  -h --help     Show this screen.
  --version     Show version.
`;

import { docopt } from "docopt";
import { markdownStore } from "../../infra/store/todo_list_markdown_store";
import { TodoListIdentifierUuidGenerator } from "../../infra/todo_list_identifier_uuid_generator";
import { renderTodoList, renderTodoLists } from "../../models/todo_dsl";
import { TodoListStore } from "../../models/todo_list_store";
import { ItemUsecases } from "../../usecases/item_usecases";
import { ListUsecases } from "../../usecases/list_usecases";

const store = markdownStore("./demo/db/");

const options = docopt(Help, {version: "0.1"});

if (options.list) {
  const listUsecases = new ListUsecases(store, new TodoListIdentifierUuidGenerator());
  if (options.create) {
    console.log(renderTodoList(listUsecases.createList(options["<name>"])));
  } else if (options.all) {
    console.log(renderTodoLists(listUsecases.all()));
  } else if (options.rename) {
    console.log(listUsecases.renameList(options["<id>"], options["<name>"])
      .map((list) => renderTodoList(list))
      .valueOr("Nothing done :("));
  } else if (options.delete) {
    listUsecases.deleteList(options["<id>"]);
  }
} else if (options.item) {
  const itemUsecases = new ItemUsecases(store);
  if (options.create) {
    console.log(itemUsecases.createItem(options["<id>"], options["<label>"])
      .map((list) => renderTodoList(list))
      .valueOr("Nothing done :("));
  } else if (options.delete) {
    console.log(itemUsecases.deleteItem(options["<id>"], options["<index>"])
      .map((list) => renderTodoList(list))
      .valueOr("Nothing done :("));
  } else if (options.toggle) {
    console.log(itemUsecases.toggleItem(options["<id>"], options["<index>"])
      .map((list) => renderTodoList(list))
      .valueOr("Nothing done :("));
  }
}
