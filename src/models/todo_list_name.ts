
export class TodoListName {
  public static fromString(value: string): TodoListName {
    return new TodoListName(value);
  }

  private constructor(public readonly value: string) {
  }
}
