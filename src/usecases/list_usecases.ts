import { List } from "immutable";
import { Maybe } from "tsmonad";
import { TodoList } from "../models/todo_list";
import { TodoListIdentifier } from "../models/todo_list_identifier";
import { TodoListIdentifierGenerator } from "../models/todo_list_identifier_generator";
import { TodoListName } from "../models/todo_list_name";
import { TodoListStore } from "../models/todo_list_store";

export class ListUsecases {
  public constructor(private readonly store: TodoListStore, private readonly generator: TodoListIdentifierGenerator) {
  }

  public createList(name: string): TodoList {
    return this.store.save(TodoList.create(this.generator.generate(), TodoListName.fromString(name)));
  }

  public renameList(identifier: string, newName: string): Maybe<TodoList> {
    return this.store
      .findById(TodoListIdentifier.fromString(identifier))
      .map((list) => list.rename(TodoListName.fromString(newName)))
      .do({just: (list) => this.store.save(list)});
  }

  public all(): List<TodoList> {
    return this.store.all();
  }

  public deleteList(identifier: string): void {
    this.store.delete(TodoListIdentifier.fromString(identifier));
  }
}
