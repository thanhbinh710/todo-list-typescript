const IdentifierRegexp = /^[0-9a-f\-]+$/i;

export class TodoListIdentifier {
  public static fromString(value: string): TodoListIdentifier {
    if (value.match(IdentifierRegexp)) {
      return new TodoListIdentifier(value);
    }
    throw new TypeError(`${value} is not a valid todo list identifier`);
  }

  private constructor(public readonly value: string) {
  }
}
