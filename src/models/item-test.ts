import { expect } from "chai";
import { Item } from "./item";

describe("item", () => {

  describe("check", () => {
    it("should be unchecked by default", () => {
      expect(Item.create("item").isChecked).to.equal(false);
    });

    it("should set the check property to true", () => {
      expect(Item.create("item").check().isChecked).to.equal(true);
    });

    it("should make a copy", () => {
      const item = Item.create("item");
      item.check();
      expect(item.isChecked).to.equal(false);
    });
  });

  describe("uncheck", () => {
    it("should set the check property to false", () => {
      expect(Item.create("item").check().uncheck().isChecked).to.equal(false);
    });
  });

  describe("toggle", () => {
    it("should set invert the check property", () => {
      expect(Item.create("item").toggle().isChecked).to.equal(true);
      expect(Item.create("item").toggle().toggle().isChecked).to.equal(false);
    });
  });
});
