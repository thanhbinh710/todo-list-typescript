
export class ItemIdentifier {
  public static of(value: number): ItemIdentifier {
    if (value >= 0) {
      return new ItemIdentifier(value);
    }
    throw new TypeError(`${value} is not a valid item identifier`);
  }

  private constructor(public readonly value: number) {
  }
}
