import { Maybe } from "tsmonad";
import { ItemIdentifier } from "../models/item_identifier";
import { TodoList } from "../models/todo_list";
import { TodoListIdentifier } from "../models/todo_list_identifier";
import { TodoListStore } from "../models/todo_list_store";

export class ItemUsecases {
  public constructor(private readonly store: TodoListStore) {
  }

  public createItem(identifier: string, label: string): Maybe<TodoList> {
    return this.withList(identifier, (list) => list.createItem(label));
  }

  public checkItem(identifier: string, index: number): Maybe<TodoList> {
    return this.withList(identifier, (list) => list.check(ItemIdentifier.of(index)));
  }

  public toggleItem(identifier: string, index: number): Maybe<TodoList> {
    return this.withList(identifier, (list) => list.toggle(ItemIdentifier.of(index)));
  }

  public uncheckItem(identifier: string, index: number): Maybe<TodoList> {
    return this.withList(identifier, (list) => list.uncheck(ItemIdentifier.of(index)));
  }

  public deleteItem(identifier: string, index: number): Maybe<TodoList> {
    return this.withList(identifier, (list) => list.delete(ItemIdentifier.of(index)));
  }

  private withList(identifier: string, functor: (list: TodoList) => TodoList): Maybe<TodoList> {
    return this.store
      .findById(TodoListIdentifier.fromString(identifier))
      .map(functor)
      .do({just: (list) => this.store.save(list)});
  }

}
