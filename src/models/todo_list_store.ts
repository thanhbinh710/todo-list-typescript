import { List, Map } from "immutable";
import { Maybe } from "tsmonad";
import { TodoList } from "./todo_list";
import { TodoListIdentifier } from "./todo_list_identifier";

export interface TodoListStore {
  all(): List<TodoList>;
  findById(identifier: TodoListIdentifier): Maybe<TodoList>;
  save(list: TodoList): TodoList;
  delete(identifier: TodoListIdentifier): void;
}
