import { expect } from "chai";
import { renderStore } from "../../testhelpers/store_dsl";
import { TemporaryFilesystem } from "../../testhelpers/temporary_filesystem";
import { unindent } from "../../testhelpers/unindent";
import { markdownStore } from "./todo_list_markdown_store";

describe("todo list filesystem store", () => {

  let filesystem: TemporaryFilesystem;

  before(() => {
    filesystem = TemporaryFilesystem.create();
  });

  after(() => {
    filesystem.destroy();
  });

  it("should load todo lists from filesystem", () => {
    filesystem
      .withFile("04bc1c20-0833-11e9-ae6d-9721e8f6b101.todo", `
        |[04bc1c20-0833-11e9-ae6d-9721e8f6b101] computers:
        |  - [x] Dell
        |  - [ ] Asus
        |  - [x] HP
        |  - [ ] Apple
      `)
      .withFile("92efee24-0831-11e9-8598-9b69d3cfecff.todo", `
        |[92efee24-0831-11e9-8598-9b69d3cfecff] os:
        |  - [ ] Windows XP
        |  - [x] Debian
        |  - [x] Ubuntu
        |  - [ ] Redhat
        |  - [ ] MacOS
      `);

    expect(renderStore(markdownStore(filesystem.base))).to.equal(unindent(`
      |[04bc1c20-0833-11e9-ae6d-9721e8f6b101] computers:
      |  - [x] Dell
      |  - [ ] Asus
      |  - [x] HP
      |  - [ ] Apple
      |
      |[92efee24-0831-11e9-8598-9b69d3cfecff] os:
      |  - [ ] Windows XP
      |  - [x] Debian
      |  - [x] Ubuntu
      |  - [ ] Redhat
      |  - [ ] MacOS
    `));
  });

});
