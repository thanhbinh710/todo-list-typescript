
export class Item {
  public static create(label: string): Item {
    return new Item(label, false);
  }

  private constructor(public readonly label: string, public readonly isChecked: boolean) {
  }

  public check(): Item {
    return new Item(this.label, true);
  }

  public uncheck(): Item {
    return new Item(this.label, false);
  }

  public toggle(): Item {
    return new Item(this.label, !this.isChecked);
  }
}
