import { List } from "immutable";
import { generate } from "pegjs";
import { Item } from "../models/item";
import { TodoList } from "../models/todo_list";
import { TodoListIdentifier } from "../models/todo_list_identifier";
import { TodoListName } from "../models/todo_list_name";

export function renderTodoLists(lists: List<TodoList>): string {
  return lists.map((list) => renderTodoList(list)).join("\n");
}

export function renderTodoList(list: TodoList): string {
  return `[${list.identifier.value}] ${list.name.value}:\n${renderItems(list.items)}`;
}

export function renderItems(items: List<Item>): string {
  if (items.size > 0) {
    return items.map(renderItem).join("");
  }
  return "  <empty>\n";
}

function renderItem(item: Item): string {
  return (item.isChecked ? "  - [x] " : "  - [ ] ") + item.label + "\n";
}

export function parseTodoLists(definitions: string): List<TodoList> {
  return List(TodoListParser.parse(definitions, {startRule: "lists"}).map(toList));
}

export function parseTodoList(definitions: string): TodoList {
  return toList(TodoListParser.parse(definitions, {startRule: "list"}));
}

export function parseItems(definitions: string): List<Item> {
  return List(TodoListParser.parse(definitions, {startRule: "items"}).map(toItem));
}

function toList(list: any): TodoList {
  return TodoList
    .create(
      TodoListIdentifier.fromString(list.identifier),
      TodoListName.fromString(list.name))
    .withItems(List(list.items.map(toItem)));
}

function toItem(item: any): Item {
  const result = Item.create(item.label);
  return item.isChecked ? result.check() : result;
}

const TodoListParser = generate(`
  lists = emptylines lists:list* { return lists; }

  list
    = _ "[" uuid:uuid "]" _ name:name ":" eol* items:items {
      return {'identifier': uuid, 'name': name, 'items': items}; }

  items
    = _ "<empty>" eol* { return []; }
    / item+

  item
    = _ "-" _ check:status _ label:label eol* { return {'isChecked': check, 'label': label}; }

  status
    = "[" check:("x" / " ") "]" { return check === "x"; }

  label
    = $([^\\r\\n]*)

  name "<name>"
    = $[^:]+

  uuid "<uuid>"
    = $[0-9a-f\-]+

  eol "<eol>"
    = [\\r\\n]

  emptylines
    = (_ eol)*

  _ "whitespace"
    = [ \t]*
`, {allowedStartRules: ["lists", "list", "items"]});
