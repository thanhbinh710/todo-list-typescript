import { List } from "immutable";
import { Maybe } from "tsmonad";
import { Item } from "./item";
import { ItemIdentifier } from "./item_identifier";
import { TodoListIdentifier } from "./todo_list_identifier";
import { TodoListName } from "./todo_list_name";

export class TodoList {
  public static create(identifier: TodoListIdentifier, name: TodoListName): TodoList {
    return new TodoList(identifier, name, List());
  }

  private constructor(
    public readonly identifier: TodoListIdentifier,
    public readonly name: TodoListName,
    public readonly items: List<Item>) {
  }

  public rename(name: TodoListName): TodoList {
    return new TodoList(this.identifier, name, this.items);
  }

  public createItem(label: string): TodoList {
    return this.withItems(this.items.push(Item.create(label)));
  }

  public item(itemIdentifier: ItemIdentifier): Maybe<Item> {
    return this.index(itemIdentifier).map((i) => this.items.get(i));
  }

  public delete(itemIdentifier: ItemIdentifier): TodoList {
    return this.onIndex(itemIdentifier, (i) => this.items.delete(i));
  }

  public check(itemIdentifier: ItemIdentifier): TodoList {
    return this.onItem(itemIdentifier, (item) => item.check());
  }

  public uncheck(itemIdentifier: ItemIdentifier): TodoList {
    return this.onItem(itemIdentifier, (item) => item.uncheck());
  }

  public toggle(itemIdentifier: ItemIdentifier): TodoList {
    return this.onItem(itemIdentifier, (item) => item.toggle());
  }

  public withItems(items: List<Item>): TodoList {
    if (items.equals(this.items)) {
      return this;
    }
    return new TodoList(this.identifier, this.name, items);
  }

  private index(itemIdentifier: ItemIdentifier): Maybe<number> {
    if (itemIdentifier.value < 0 || itemIdentifier.value >= this.items.size) {
      return Maybe.nothing();
    }
    return Maybe.just(itemIdentifier.value);
  }

  private onIndex(itemIdentifier: ItemIdentifier, functor: (index: number) => List<Item>): TodoList {
    return this.withItems(this.index(itemIdentifier).map(functor).valueOr(this.items));
  }

  private onItem(itemIdentifier: ItemIdentifier, functor: (item: Item) => Item): TodoList {
    return this.onIndex(itemIdentifier, (index) => this.items.set(index, functor(this.items.get(index))));
  }
}
